package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();


	public static IntegersBag createBag(ArrayList<Integer> values){
		return new IntegersBag(values);		
	}


	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}

	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}

	public static double darMinimo(IntegersBag bag)
	{
		return model.darMinimo(bag);
	}

	public static double darSuma(IntegersBag bag)
	{
		return model.darSuma(bag);
	}

	public static double darCuenta(IntegersBag bag)
	{
		return model.darCuenta(bag);
	}

}