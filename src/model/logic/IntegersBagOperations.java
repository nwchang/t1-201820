package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int darMinimo (IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int valor;
		if (bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext())
			{
				valor = iter.next();
				if (min > valor)
				{
					min = valor;
				}
			}
		}
		return min;
	}

	public int darSuma (IntegersBag bag)
	{
		int suma = 0;
		int valor;
		if (bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext())
			{
				valor = iter.next();
				suma += valor;
			}
		}
		return suma;
	}

	public int darCuenta (IntegersBag bag)
	{
		int cuenta = 0;
		int valor;
		if (bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext())
			{
				valor = iter.next();
				cuenta++;
			}
		}
		return cuenta;
	}

}

